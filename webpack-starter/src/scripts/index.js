import "../styles/index.scss";
import * as moment from "moment";

console.log(moment().format("MMMM Do YYYY, h:mm:ss a"));

// Task 1
//#region Task 1
var f1 = 0;
var f2 = 1;
var i, f3;
for (i = 2; i <= 50; i++) {
	f3 = f1 + f2;
	console.log("", f3, " ");
	f1 = f2;
	f2 = f3;
}
//#endregion

// Task 2
//#region Task 2
for (var i = 1; i <= 50; i++) {
	if (i % 15 == 0) console.log("FizzBuzz");
	else if (i % 5 == 0) console.log("Buzz");
	else if (i % 3 == 0) console.log("Fizz");
	else console.log(i);
}
//#endregion

//#region Task JS Clock
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var radius = canvas.height / 2;
ctx.translate(radius, radius);
radius = radius * 0.9;
setInterval(drawClock, 1000);

function drawClock() {
	drawFace(ctx, radius);
	drawNumbers(ctx, radius);
	drawTime(ctx, radius);
}

function drawFace(ctx, radius) {
	var grad;
	ctx.beginPath();
	ctx.arc(0, 0, radius, 0, 2 * Math.PI);
	ctx.fillStyle = "white";
	ctx.fill();
	grad = ctx.createRadialGradient(0, 0, radius * 0.95, 0, 0, radius * 1.05);
	grad.addColorStop(0, "#333");
	grad.addColorStop(0.5, "white");
	grad.addColorStop(1, "#333");
	ctx.strokeStyle = grad;
	ctx.lineWidth = radius * 0.1;
	ctx.stroke();
	ctx.beginPath();
	ctx.arc(0, 0, radius * 0.1, 0, 2 * Math.PI);
	ctx.fillStyle = "#333";
	ctx.fill();
}

function drawNumbers(ctx, radius) {
	var ang;
	var num;
	ctx.font = radius * 0.15 + "px arial";
	ctx.textBaseline = "middle";
	ctx.textAlign = "center";
	for (num = 1; num < 13; num++) {
		ang = (num * Math.PI) / 6;
		ctx.rotate(ang);
		ctx.translate(0, -radius * 0.85);
		ctx.rotate(-ang);
		ctx.fillText(num.toString(), 0, 0);
		ctx.rotate(ang);
		ctx.translate(0, radius * 0.85);
		ctx.rotate(-ang);
	}
}

function drawTime(ctx, radius) {
	// using moment.js to get hour, minute, second
	var hour = moment().hour();
	var minute = moment().minute();
	var second = moment().second();
	//hour
	hour = hour % 12;
	hour = (hour * Math.PI) / 6 + (minute * Math.PI) / (6 * 60) + (second * Math.PI) / (360 * 60);
	drawHand(ctx, hour, radius * 0.5, radius * 0.07);
	//minute
	minute = (minute * Math.PI) / 30 + (second * Math.PI) / (30 * 60);
	drawHand(ctx, minute, radius * 0.8, radius * 0.07);
	// second
	second = (second * Math.PI) / 30;
	drawHand(ctx, second, radius * 0.9, radius * 0.02);
}

function drawHand(ctx, pos, length, width) {
	ctx.beginPath();
	ctx.lineWidth = width;
	ctx.lineCap = "round";
	ctx.moveTo(0, 0);
	ctx.rotate(pos);
	ctx.lineTo(0, -length);
	ctx.stroke();
	ctx.rotate(-pos);
}
//#endregion

// Task Calculator
//#region Task Calc
const calculator = {
	displayValue: "0",
	firstNumber: null,
	WaitingForSecondNumber: false,
	operator: null
};

function inputNumber(number) {
	const { displayValue, WaitingForSecondNumber: WaitingForSecondNumber } = calculator;

	if (WaitingForSecondNumber === true) {
		calculator.displayValue = number;
		calculator.WaitingForSecondNumber = false;
	} else {
		calculator.displayValue = displayValue === "0" ? number : displayValue + number;
	}
}

function handleOperator(nextOperator) {
	const { firstNumber: firstNumber, displayValue, operator } = calculator;
	const inputValue = parseFloat(displayValue);

	if (operator && calculator.WaitingForSecondNumber) {
		calculator.operator = nextOperator;
		return;
	}

	if (firstNumber == null) {
		calculator.firstNumber = inputValue;
	} else if (operator) {
		const currentValue = firstNumber || 0;
		const result = performCalculation[operator](currentValue, inputValue);

		calculator.displayValue = String(result);
		calculator.firstNumber = result;
	}

	calculator.WaitingForSecondNumber = true;
	calculator.operator = nextOperator;
}

const performCalculation = {
	"/": (firstNumber, secondNumber) => firstNumber / secondNumber,

	"*": (firstNumber, secondNumber) => firstNumber * secondNumber,

	"+": (firstNumber, secondNumber) => firstNumber + secondNumber,

	"-": (firstNumber, secondNumber) => firstNumber - secondNumber,

	"=": (firstNumber, secondNumber) => secondNumber
};

function resetCalculator() {
	calculator.displayValue = "0";
	calculator.firstNumber = null;
	calculator.WaitingForSecondNumber = false;
	calculator.operator = null;
}

function updateDisplay() {
	const display = document.querySelector(".calculatorOutput");
	display.value = calculator.displayValue;
}

updateDisplay();

const keys = document.querySelector(".calculatorInputs");
keys.addEventListener("click", event => {
	const { target } = event;
	if (!target.matches("button")) {
		return;
	}

	if (target.classList.contains("operator")) {
		handleOperator(target.value);
		updateDisplay();
		return;
	}

	if (target.classList.contains("all-clear")) {
		resetCalculator();
		updateDisplay();
		return;
	}

	inputNumber(target.value);
	updateDisplay();
});

//#endregion

// Task Rock, Paper, Scissors
//#region Rock, paper, scissors game
var userChoice = document.getElementById("userInput");

// Function for assigning a choice to the computer
function computerChoice() {
	//Math.random returns a random number between 0 and 1
	var computerChoice = Math.random();
	if (computerChoice < 0.34) {
		computerChoice = "rock";
	} else if (computerChoice <= 0.67) {
		computerChoice = "paper";
	} else {
		computerChoice = "scissors";
	}
	return computerChoice;
}

var compareChoices = function(firstChoice, secondChoice) {
	if (firstChoice === secondChoice) {
		return "It's a tie!";
	} else if (firstChoice === "rock") {
		if (secondChoice === "scissors") {
			return "Rock wins <br /> You win!";
		} else {
			return "Paper wins <br /> The computer wins.";
		}
	} else if (firstChoice === "paper") {
		if (secondChoice === "rock") {
			return "Paper wins <br /> You win!";
		} else {
			return "Scissors wins <br /> The computer wins.";
		}
	} else if (firstChoice === "scissors") {
		if (secondChoice === "rock") {
			return "Rock wins <br /> The computer wins.";
		} else {
			return "Scissors wins <br /> You win!";
		}
	}
};

document.getElementById("submit").onclick = function() {
	submitBtnClicked();
};

function submitBtnClicked() {
	var computerAnswer = computerChoice();
	document.getElementById("outputComputer").innerHTML =
		"Computer chose: " + computerAnswer + "<br>";
	document.getElementById("outputGame").innerHTML = compareChoices(
		userChoice.value.toLowerCase(),
		computerAnswer
	);
}
//#endregion
